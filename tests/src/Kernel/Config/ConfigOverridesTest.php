<?php

namespace Drupal\Tests\config_pages_overrides\Kernel\Config;

use Drupal\config_pages\Entity\ConfigPages;
use Drupal\config_pages\Entity\ConfigPagesType;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\KernelTests\KernelTestBase;

/**
 * Class ConfigOverridesTest
 *
 * @group config_pages_overrides
 * @coversDefaultClass \Drupal\config_pages_overrides\Config\ConfigOverrides
 */
class ConfigOverridesTest extends KernelTestBase {

  /**
   * {@inheritDoc}
   */
  protected static $modules = [
    'system',
    'config_pages',
    'config_pages_overrides',
    'field',
  ];

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->installEntitySchema('config_pages_type');
    $this->installEntitySchema('config_pages');
    $this->installConfig('system');
    \Drupal::configFactory()
      ->getEditable('system.site')
      ->set('name', $this->randomMachineName())
      ->save();

    $override_settings = [
      'field' => 'field_foo',
      'delta' => 0,
      'column' => 'value',
      'config_name' => 'system.site',
      'config_item' => 'name',
    ];

    $config_page_type = ConfigPagesType::create([
      'id' => 'foo',
      'menu' => [],
      'context' => [],
    ]);
    $config_page_type->setThirdPartySetting('config_pages_overrides', $this->randomMachineName(), $override_settings);
    $config_page_type->save();

    $field_storage = FieldStorageConfig::create([
      'type' => 'string',
      'field_name' => 'field_foo',
      'entity_type' => 'config_pages',
    ]);
    $field_storage->save();
    FieldConfig::create([
      'field_storage' => $field_storage,
      'bundle' => 'foo',
      'lable' => 'name',
    ])->save();
  }

  /**
   * Test config is overridden.
   */
  public function testConfigOverride() {
    $page = ConfigPages::create([
      'type' => 'foo',
      'field_foo' => 'Foo Bar',
      'context' => 'a:0:{}',
    ]);
    $page->save();
    drupal_flush_all_caches();
    $this->assertEquals('Foo Bar', \Drupal::config('system.site')->get('name'));
  }

}

<?php

namespace Drupal\config_pages_overrides\Form;

use Drupal\Component\Uuid\UuidInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\field\Entity\FieldStorageConfig;
use Drupal\field\FieldConfigInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPagesOverridesForm.
 *
 * @package Drupal\config_pages_overrides\Form
 */
class ConfigPagesOverridesAddForm extends EntityForm {

  /**
   * Config storage service.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $configStorage;

  /**
   * UUID Service.
   *
   * @var \Drupal\Component\Uuid\UuidInterface
   */
  protected $uuidGenerator;

  /**
   * Cache service on render bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $renderCache;

  /**
   * Cache service on config bin.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected $configCache;

  /**
   * Array of config entity types on the site.
   *
   * @var \Drupal\Core\Config\Entity\ConfigEntityTypeInterface[]
   */
  protected $configEntityTypeDefinitions;

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('config.factory'),
      $container->get('config.storage'),
      $container->get('uuid'),
      $container->get('cache.render'),
      $container->get('cache.config')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager, ConfigFactoryInterface $config_factory, StorageInterface $config_storage, UuidInterface $uuid, CacheBackendInterface $render_cache, CacheBackendInterface $config_cache) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->configStorage = $config_storage;
    $this->uuidGenerator = $uuid;
    $this->renderCache = $render_cache;
    $this->configCache = $config_cache;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $config_type = $form_state->getValue('config_type');
    $config_name = $form_state->getValue('config_name');
    $field = $form_state->getValue('field');

    /** @var \Drupal\Core\Entity\EntityFieldManager $entityFieldManager */
    $entityFieldManager = \Drupal::service('entity_field.manager');
    $field_definitions = $entityFieldManager->getFieldDefinitions('config_pages', $this->entity->id());

    $field = $field ? $field_definitions[$field] : NULL;

    $field_options = array_filter($field_definitions, function (FieldDefinitionInterface $field) {
      return $field instanceof FieldConfigInterface;
    });
    $field_options = array_map(function (FieldDefinitionInterface $field) {
      return $field->getLabel();
    }, $field_options);

    $form['field'] = [
      '#type' => 'select',
      '#title' => $this->t('Field'),
      '#options' => $field_options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changeField',
        'wrapper' => 'edit-field-wrapper',
      ],
    ];
    $form['field_settings'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-field-wrapper">',
      '#suffix' => '</div>',
    ];

    $cardinality = $field ? $field->getFieldStorageDefinition()
      ->getCardinality() : 0;
    $form['field_settings']['delta'] = [
      '#type' => 'number',
      '#title' => $this->t('Delta'),
      '#min' => min(0, $cardinality),
      '#max' => $cardinality == FieldStorageConfig::CARDINALITY_UNLIMITED ? 99 : $cardinality - 1,
      '#default_value' => 0,
      '#required' => TRUE,
    ];
    $column_options = $field ? $field->getFieldStorageDefinition()
      ->getColumns() : [];

    array_walk($column_options, function (&$column_schema, $column_key) {
      $label = ucfirst($column_key);
      if (isset($column_schema['description'])) {
        $label .= ': ' . $column_schema['description'];
      }
      $column_schema = $label;
    });

    $form['field_settings']['column'] = [
      '#type' => 'select',
      '#title' => $this->t('Field Value'),
      '#options' => $column_options,
      '#required' => TRUE,
    ];

    $entity_types = array_map(function (EntityTypeInterface $definition) {
      return $definition->getLabel();
    }, $this->getConfigEntityTypeDefinitions());
    // Sort the entity types by label.
    uasort($entity_types, 'strnatcasecmp');
    // Add the simple configuration type to the top of the list.
    $config_types = ['system.simple' => $this->t('Simple configuration')] + $entity_types;

    $form['config_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration type'),
      '#options' => $config_types,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changeConfigObject',
        'wrapper' => 'edit-config-object-wrapper',
      ],
    ];

    $form['config_object'] = [
      '#type' => 'container',
      '#prefix' => '<div id="edit-config-object-wrapper">',
      '#suffix' => '</div>',
    ];

    $form['config_object']['config_name'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration name'),
      '#options' => $this->getConfigNames($config_type),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::changeConfigObject',
        'wrapper' => 'edit-config-object-wrapper',
      ],
    ];

    $form['config_object']['config_item'] = [
      '#type' => 'select',
      '#title' => $this->t('Configuration item'),
      '#options' => $this->getConfigItems($config_type, $config_name),
      '#required' => TRUE,
    ];

    $form['prefix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value Prefix'),
      '#description' => $this->t('Add some text before the field value.'),
    ];

    $form['suffix'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Value Suffix'),
      '#description' => $this->t('Add some text after the field value.'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_type = $form_state->getValue('config_type');
    $config_name = $form_state->getValue('config_name');
    if (isset($this->getConfigEntityTypeDefinitions()[$config_type])) {
      $config_name = $this->entityTypeManager->getStorage($config_type)
        ->load($config_name)
        ->getConfigDependencyName();
    }

    $settings = [
      'field' => $form_state->getValue('field'),
      'delta' => $form_state->getValue('delta'),
      'column' => $form_state->getValue('column'),
      'config_name' => $config_name,
      'config_item' => $form_state->getValue('config_item'),
      'prefix' => $form_state->getValue('prefix'),
      'suffix' => $form_state->getValue('suffix'),
    ];

    $this->entity->setThirdPartySetting('config_pages_overrides', $this->uuidGenerator->generate(), $settings);
    $form_state->setRedirect('entity.config_pages_type.config_overrides_form', ['config_pages_type' => $this->entity->id()]);

    $this->renderCache->invalidateAll();
    $this->configCache->invalidate($settings['config_name']);
  }

  /**
   * Updates the field settings container element.
   *
   * @param array $form
   *   Complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state.
   *
   * @return array
   *   Render array to replace.
   */
  public function changeField(array $form, FormStateInterface $form_state) {
    return $form['field_settings'];
  }

  /**
   * Updates the configuration object container element.
   *
   * @param array $form
   *   Complete form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Current state.
   *
   * @return array
   *   Render array to replace.
   */
  public function changeConfigObject(array $form, FormStateInterface $form_state) {
    return $form['config_object'];
  }

  /**
   * Get the configuration entity type definitions.
   *
   * @param bool $with_excluded
   *   Whether or not to include excluded configuration types.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   The entity type definitions.
   */
  protected function getConfigEntityTypeDefinitions($with_excluded = FALSE) {
    if (!isset($this->configEntityTypeDefinitions)) {
      $config_entity_type_definitions = [];
      foreach ($this->entityTypeManager->getDefinitions() as $entity_type => $definition) {
        if ($definition->entityClassImplements(ConfigEntityInterface::class)) {
          $config_entity_type_definitions[$entity_type] = $definition;
        }
      }
      $this->configEntityTypeDefinitions = $config_entity_type_definitions;
    }

    if ($with_excluded) {
      $definitions = $this->configEntityTypeDefinitions;
    }
    else {
      $definitions = array_diff_key($this->configEntityTypeDefinitions, $this->excludedConfigTypes());
    }
    return $definitions;
  }

  /**
   * Define the list of configuration types to exclude.
   *
   * @return array
   *  The configuration types to exclude.
   */
  protected function excludedConfigTypes() {
    $exclude = ['config_pages_type', 'config_pages'];
    return array_combine($exclude, $exclude);
  }

  /**
   * Get the configuration names for a specified configuration type.
   *
   * @param string|null $config_type
   *   The configuration type.
   *
   * @return array
   *   The configuration names.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getConfigNames($config_type = NULL) {
    $names = [
      '' => $this->t('- Select -'),
    ];

    // Handle entity configuration types.
    if ($config_type && $config_type !== 'system.simple') {
      $entity_storage = $this->entityTypeManager->getStorage($config_type);
      foreach ($entity_storage->loadMultiple() as $entity) {
        $entity_id = $entity->id();
        if ($label = $entity->label()) {
          $names[$entity_id] = $this->t('@label (@id)', [
            '@label' => $label,
            '@id' => $entity_id,
          ]);
        }
        else {
          $names[$entity_id] = $entity_id;
        }
      }
    }
    // Handle simple configuration.
    elseif ($config_type == 'system.simple') {
      // Gather the configuration entity prefixes.
      $config_prefixes = array_map(function (EntityTypeInterface $definition) {
        return $definition->getConfigPrefix() . '.';
      }, $this->getConfigEntityTypeDefinitions(TRUE));

      // Get all configuration names.
      $names = $this->configStorage->listAll();
      $names = array_combine($names, $names);

      // Filter out any names that match a configuration entity prefix
      foreach ($names as $config_name) {
        foreach ($config_prefixes as $config_prefix) {
          if (strpos($config_name, $config_prefix) === 0) {
            unset($names[$config_name]);
          }
        }
      }
    }

    return $names;
  }

  /**
   * Get the configuration items for a specified configuration name.
   *
   * @param string|null $config_type
   *   The configuration type.
   * @param string|null $config_name
   *   The configuration name.
   *
   * @return array
   *   The configuration items.
   */
  protected function getConfigItems($config_type = NULL, $config_name = NULL) {
    $config_items = [];

    if (!$config_name) {
      return $config_items;
    }

    // For simple configuration, use the configuration name. For configuration
    // entities, use a combination of the prefix and configuration name.
    if ($config_type == 'system.simple') {
      $name = $config_name;
    }
    else {
      $definition = $this->getConfigEntityTypeDefinitions()[$config_type];
      $name = $definition->getConfigPrefix() . '.' . $config_name;
    }

    $config_object = $this->configFactory->get($name);
    $config_array = $config_object->get();
    $config_items += $this->flattenConfigItemList($config_array);
    $config_items = array_combine($config_items, $config_items);

    return $config_items;
  }

  /**
   * Recursively create a flat array of configuration items.
   *
   * @param $config_array
   *   An array of configuration items.
   * @param string $prefix
   *   A prefix to add to nested items.
   * @param int $level
   *   The current level of nesting.
   *
   * @return array
   *   The flattened array of configuration items.
   */
  protected function flattenConfigItemList($config_array, $prefix = '', $level = 0) {
    $config_items = [];

    // Define items to ignore.
    $ignore = [
      'uuid',
      '_core',
    ];

    foreach ($config_array as $key => $value) {
      if (in_array($key, $ignore) && $level == 0) {
        continue;
      }

      if (is_array($value) && $level < 5) {
        $config_items = array_merge($config_items, $this->flattenConfigItemList($value, $prefix . $key . '.', $level + 1));
      }
      else {
        $config_items[] = rtrim($prefix, ".");
        $config_items[] = $prefix . $key;
      }
    }

    return $config_items;
  }

}

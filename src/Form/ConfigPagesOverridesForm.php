<?php

namespace Drupal\config_pages_overrides\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ConfigPagesOverridesForm.
 *
 * @package Drupal\config_pages_overrides\Form
 */
class ConfigPagesOverridesForm extends EntityForm {

  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $options = $this->entity->getThirdPartySettings('config_pages_overrides', []);
    $field_storage = $this->entityTypeManager->getStorage('field_config');
    foreach ($options as &$line) {
      if ($field = $field_storage->load("config_pages.{$this->entity->id()}.{$line['field']}")) {
        $line['field'] = $field->label();
      }

      if (!empty($line['prefix']) || !empty($line['suffix'])) {
        $line['column'] = $line['prefix'] . '[' . $line['column'] . ']' . $line['suffix'];
      }
    }

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $this->getTableHeader(),
      '#options' => $options,
      '#empty' => $this->t('No configuration overrides created'),
    ];
    return $form;
  }

  /**
   * Get the form table header.
   *
   * @return array
   */
  protected function getTableHeader() {
    return [
      'field' => $this->t('Field'),
      'delta' => $this->t('Delta'),
      'column' => $this->t('Field Column'),
      'config_name' => $this->t('Config'),
      'config_item' => $this->t('Config Item'),
    ];
  }

  /**
   * {@inheritDoc}
   */
  protected function actions(array $form, FormStateInterface $form_state) {
    $actions['delete'] = [
      '#type' => 'submit',
      '#value' => $this->t('Delete'),
      '#submit' => ['::deleteOverride'],
    ];
    return $actions;
  }

  /**
   * Deletes the selected override configuration.
   *
   * @param array $form
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteOverride(array &$form, FormStateInterface $form_state) {
    foreach (array_filter($form_state->getValue('table')) as $uuid) {
      $this->entity->unsetThirdPartySetting('config_pages_overrides', $uuid);
    }
    $this->entity->save();
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Intentionally left empty to avoid re-saving the config pages type.
  }

}

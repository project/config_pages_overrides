<?php

namespace Drupal\config_pages_overrides\Config;

use Drupal\Component\Utility\NestedArray;
use Drupal\config_pages\ConfigPagesLoaderServiceInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\Config\ConfigFactoryOverrideInterface;
use Drupal\Core\Config\Schema\Element;
use Drupal\Core\Config\StorageInterface;
use Drupal\Core\Installer\InstallerKernel;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Configuration overrides for the profile.
 *
 * @package Drupal\stanford_profile_config_overrides\Config
 */
class ConfigOverrides implements ConfigFactoryOverrideInterface {

  /**
   * Config Pages service.
   *
   * @var \Drupal\config_pages\ConfigPagesLoaderServiceInterface
   */
  protected $configPagesLoader;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Existing config pages types.
   *
   * @var \Drupal\config_pages\ConfigPagesTypeInterface[]
   */
  protected $configPagesTypes;

  /**
   * ConfigOverrides constructor.
   *
   * @param \Drupal\config_pages\ConfigPagesLoaderServiceInterface $config_pages
   */
  public function __construct(ConfigPagesLoaderServiceInterface $config_pages) {
    $this->configPagesLoader = $config_pages;
    $this->configPagesTypes = self::getConfigPages();
  }

  /**
   * Load the config pages that exist.
   *
   * Use a static method instead of dependency injection to avoid circular
   * dependencies.
   *
   * @return \Drupal\config_pages\ConfigPagesTypeInterface[]
   *   Keyed array of config page type entities.
   */
  protected static function getConfigPages() {
    try {
      return \Drupal::entityTypeManager()
        ->getStorage('config_pages_type')
        ->loadMultiple();
    }
    catch (\Exception $e) {
      return [];
    }
  }

  /**
   * {@inheritDoc}
   */
  public function loadOverrides($names) {
    $overrides = [];

    foreach ($this->configPagesTypes as $page_type) {
      foreach ($page_type->getThirdPartySettings('config_pages_overrides') as $settings) {
        if (in_array($settings['config_name'], $names)) {
          $path = [$settings['config_name']];
          $path = array_merge($path, explode('.', $settings['config_item']));

          // If overriding a single value grab and use the delta.
          if ($settings['delta'] == FieldStorageConfig::CARDINALITY_UNLIMITED) {
            // If overriding an array/map grab all the values.
            $override_value = $this->configPagesLoader->getValue($page_type->id(), $settings['field'], [], $settings['column']);
            $override_value = is_array($override_value) ? array_filter($override_value) : $override_value;
          }
          else {
            // Get the single item.
            $override_value = $this->configPagesLoader->getValue($page_type->id(), $settings['field'], $settings['delta'], $settings['column']);
          }

          // Handle data type stuff.
          if (!is_null($override_value)) {

            if (!empty($settings['prefix']) || !empty($settings['suffix']) && is_string($override_value)) {
              $override_value = $settings['prefix'] . $override_value . $settings['suffix'];
            }

            $override_value = $this->castOverrideValue($override_value, $settings['config_name'], $settings['config_item']);
            NestedArray::setValue($overrides, $path, $override_value);
          }
        }
      }
    }
    return $overrides;
  }

  /**
   * Convert the override value based on the schema definition.
   *
   * @param mixed $value
   *   Override value.
   * @param string $config_name
   *   Configuration name.
   * @param $config_item
   *   Path of configuration item.
   *
   * @return mixed
   *   Cast value.
   */
  protected function castOverrideValue($value, $config_name, $config_item) {
    try {
      $config_schema = self::getConfigSchema($config_name);
      $schema = $this->convertConfigElementToList($config_schema);
    }
    catch (\Throwable $e) {
      return $value;
    }
    if (!isset($schema[$config_item])) {
      return $value;
    }

    switch ($schema[$config_item]->getDataDefinition()['type']) {
      case 'boolean':
        return (bool) $value;

      case 'integer':
        return (int) $value ?: strlen((string) $value);
    }

    return $value;
  }

  /**
   * Get the established schema for the current config.
   *
   * @param string $config_name
   *   Config name.
   *
   * @return mixed
   *   The stored schema.
   */
  protected static function getConfigSchema($config_name) {
    return \Drupal::service('config.typed')->get($config_name);
  }

  /**
   * Gets all contained typed data properties as plain array.
   *
   * @param array|object $schema
   *   An array of config elements with key.
   *
   * @return array
   *   List of Element objects indexed by full name (keys with dot notation).
   */
  protected function convertConfigElementToList($schema) {
    $list = [];
    foreach ($schema as $key => $element) {
      if ($element instanceof Element) {
        $list[$key] = $element;
        foreach ($this->convertConfigElementToList($element) as $sub_key => $value) {
          $list[$key . '.' . $sub_key] = $value;
        }
      }
      else {
        $list[$key] = $element;
      }
    }
    return $list;
  }

  /**
   * {@inheritDoc}
   */
  public function createConfigObject($name, $collection = StorageInterface::DEFAULT_COLLECTION) {
    return NULL;
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheableMetadata($name) {
    return new CacheableMetadata();
  }

  /**
   * {@inheritDoc}
   */
  public function getCacheSuffix() {
    return 'ConfigPagesOverrides';
  }

}

INTRODUCTION
============

This module uses the [Config Pages](https://www.drupal.org/project/config_pages) 
module to override other configuration with the config pages values.
This eliminates the need to manually create an override service for each config page.

Take for example you want a config pages to allow a user to change the site name.
You can create a text field on a config page, and then use the value of that field
to override the `system.site` configuration for the `name`.


REQUIREMENTS
============
Requires config_pages module.


INSTALLATION
============

Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.


CONFIGURATION
=============
Follow the guide to [create a config pages type](https://www.drupal.org/project/config_pages).
 After adding some fields to the config page type, you can then use the Config 
 overrides tab to override configuration.

